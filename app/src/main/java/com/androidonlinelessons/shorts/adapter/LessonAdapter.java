package com.androidonlinelessons.shorts.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidonlinelessons.shorts.R;
import com.androidonlinelessons.shorts.model.Lesson;

import java.util.ArrayList;

public class LessonAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Lesson> mLessonList;

    static class ItemHolder {
        public TextView title;
    }

    public LessonAdapter(Context context, ArrayList<Lesson> lessonList) {
        mContext = context;
        mLessonList = lessonList;
    }

    @Override
    public int getCount() {
        return mLessonList.size();
    }

    @Override
    public Object getItem(int position) {
        return mLessonList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            rowView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);

            ItemHolder itemHolder = new ItemHolder();
            itemHolder.title = (TextView) rowView.findViewById(android.R.id.text1);

            rowView.setTag(itemHolder);
        }

        ItemHolder itemH = (ItemHolder) rowView.getTag();
        itemH.title.setText(mLessonList.get(position).getTitle());

        return rowView;
    }
}
