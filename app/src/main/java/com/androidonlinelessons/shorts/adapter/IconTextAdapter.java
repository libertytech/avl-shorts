package com.androidonlinelessons.shorts.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.androidonlinelessons.shorts.R;

public class IconTextAdapter extends ArrayAdapter<String> {

    // month icons
    private Integer[] iconMonthIds = {
            R.drawable.january, R.drawable.february, R.drawable.march, R.drawable.april, R
            .drawable.may, R.drawable.june, R.drawable.july,
            R.drawable.august, R.drawable.september, R.drawable.october, R.drawable.november, R
            .drawable.december
    };

    public IconTextAdapter(Context c, String[] months) {
        super(c, R.layout.month_icon_text_row, R.id.text_month, months);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = super.getView(position, convertView, parent);

        ImageView icon = (ImageView) row.findViewById(R.id.icon_month);
        icon.setImageResource(iconMonthIds[position]);

        return row;
    }
}
