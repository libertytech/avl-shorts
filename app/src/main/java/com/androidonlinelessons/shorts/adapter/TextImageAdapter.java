package com.androidonlinelessons.shorts.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidonlinelessons.shorts.R;

public class TextImageAdapter extends BaseAdapter {
    private Context context;
    private String[] monthsString;
    private Integer[] monthIcons;

    public TextImageAdapter(Context c, String[] months, Integer[] icons) {
        context = c;
        monthsString = months;
        monthIcons = icons;
    }

    @Override
    public int getCount() {
        return monthIcons.length;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = inflater.inflate(R.layout.month_title_icon, null);

            TextView textView = (TextView) gridView
                    .findViewById(R.id.grid_item_label);
            textView.setText(monthsString[position]);

            ImageView imageView = (ImageView) gridView
                    .findViewById(R.id.grid_item_icon);
            imageView.setImageResource(monthIcons[position]);
        } else {
            gridView = convertView;
        }

        return gridView;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
