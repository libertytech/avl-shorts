package com.androidonlinelessons.shorts.model;

import android.content.Intent;

public class Lesson {
    private String title;
    private Intent intent;

    public Lesson(String title, Intent intent) {
        this.title = title;
        this.intent = intent;
    }

    public String getTitle() {
        return title;
    }

    public Intent getIntent() {
        return intent;
    }
}
