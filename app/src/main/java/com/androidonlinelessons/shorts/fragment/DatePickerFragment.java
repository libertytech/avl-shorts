package com.androidonlinelessons.shorts.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // make sure calling activity implements listener
        if (!(activity instanceof DatePickerDialog.OnDateSetListener)) {
            throw new RuntimeException("Activity must implement DatePickerDialog" +
                    ".OnDateSetListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // use the current time as the default
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), (DatePickerDialog
                .OnDateSetListener) getActivity(), year, month, day);
    }
}