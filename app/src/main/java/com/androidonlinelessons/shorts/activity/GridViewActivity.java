package com.androidonlinelessons.shorts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.androidonlinelessons.shorts.R;

public class GridViewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_grid_view);

        Button gridDefaultButton = (Button) findViewById(R.id.grid_view_default_button);
        if (gridDefaultButton != null) {
            gridDefaultButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), GridViewDefaultActivity
                            .class));
                }
            });
        }

        Button gridCustomButton = (Button) findViewById(R.id.grid_view_custom_button);
        if (gridCustomButton != null) {
            gridCustomButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), GridViewCustomActivity
                            .class));
                }
            });
        }
    }


}
