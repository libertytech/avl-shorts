package com.androidonlinelessons.shorts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.androidonlinelessons.shorts.R;

public class RelativeLayoutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_layout);

        Button parentButton = (Button) findViewById(R.id.relative_layout_parent_button);
        if (parentButton != null) {
            parentButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), RelativeLayoutParentActivity
                            .class));
                }
            });
        }

        Button viewButton = (Button) findViewById(R.id.relative_layout_view_button);
        if (viewButton != null) {
            viewButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), RelativeLayoutViewActivity
                            .class));
                }
            });
        }

        Button mixedButton = (Button) findViewById(R.id.relative_layout_mixed_button);
        if (mixedButton != null) {
            mixedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), RelativeLayoutMixedActivity
                            .class));
                }
            });
        }
    }
}
