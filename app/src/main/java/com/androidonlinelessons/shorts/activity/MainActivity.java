package com.androidonlinelessons.shorts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidonlinelessons.shorts.R;
import com.androidonlinelessons.shorts.adapter.LessonAdapter;
import com.androidonlinelessons.shorts.model.Lesson;

import java.util.ArrayList;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lessonListView = (ListView) findViewById(R.id.list_lessons);

        final ArrayList<Lesson> lessonList = getLessons();
        LessonAdapter adapter = new LessonAdapter(this, lessonList);
        if (lessonListView != null) {
            lessonListView.setAdapter(adapter);

            lessonListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    startActivity(lessonList.get(position).getIntent());
                }
            });
        }
    }

    private ArrayList<Lesson> getLessons() {
        ArrayList<Lesson> lessonList = new ArrayList<>();

        lessonList.add(new Lesson(getString(R.string.activity_lifecycle), new Intent(this,
                ActivityA.class)));
        lessonList.add(new Lesson(getString(R.string.alert_dialogs), new Intent(this,
                AlertDialogsActivity.class)));
        lessonList.add(new Lesson(getString(R.string.buttons), new Intent(this,
                ButtonsActivity.class)));
        lessonList.add(new Lesson(getString(R.string.checkboxes), new Intent(this,
                CheckboxesActivity.class)));
        lessonList.add(new Lesson(getString(R.string.custom_tabs), new Intent(this,
                CustomTabsActivity.class)));
        lessonList.add(new Lesson(getString(R.string.grid_view), new Intent(this,
                GridViewActivity.class)));
        lessonList.add(new Lesson(getString(R.string.image_view), new Intent(this,
                ImageViewActivity.class)));
        lessonList.add(new Lesson(getString(R.string.linear_layout), new Intent(this,
                LinearLayoutActivity.class)));
        lessonList.add(new Lesson(getString(R.string.list_view), new Intent(this,
                ListViewActivity.class)));
        lessonList.add(new Lesson(getString(R.string.pickers), new Intent(this,
                PickersActivity.class)));
        lessonList.add(new Lesson(getString(R.string.radio_buttons), new Intent(this,
                RadioButtonsActivity.class)));
        lessonList.add(new Lesson(getString(R.string.relative_layout), new Intent(this,
                RelativeLayoutActivity.class)));
        lessonList.add(new Lesson(getString(R.string.snackbars), new Intent(this,
                SnackbarsActivity.class)));
        lessonList.add(new Lesson(getString(R.string.spinners), new Intent(this,
                SpinnersActivity.class)));
        lessonList.add(new Lesson(getString(R.string.text_fields), new Intent(this,
                TextFieldsActivity.class)));
        lessonList.add(new Lesson(getString(R.string.toasts), new Intent(this,
                ToastsActivity.class)));
        lessonList.add(new Lesson(getString(R.string.toggle_buttons), new Intent(this,
                ToggleButtonsActivity.class)));
        lessonList.add(new Lesson(getString(R.string.web_view), new Intent(this,
                WebViewActivity.class)));
        return lessonList;
    }
}
