package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidonlinelessons.shorts.R;

public class TextFieldsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_fields);

        // search action button
        EditText searchEdit = (EditText) findViewById(R.id.edit_search);
        if (searchEdit != null) {
            searchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        Toast.makeText(TextFieldsActivity.this, "Searching for " + v.getText(),
                                Toast.LENGTH_LONG)
                                .show();
                        handled = true;
                    }
                    return handled;
                }
            });
        }

        // autocomplete setup
        AutoCompleteTextView countryText = (AutoCompleteTextView) findViewById(R.id
                .auto_edit_country);
        String[] countries = getResources().getStringArray(R.array.countries_array);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout
                .simple_list_item_1, countries);
        countryText.setAdapter(adapter);
    }

}
