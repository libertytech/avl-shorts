package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;

import com.androidonlinelessons.shorts.R;

public class ImageViewCenterInsideActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view_center_inside);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
