package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidonlinelessons.shorts.R;

public class RadioButtonsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_buttons);

        // alternative way to send onClick listener
        RadioGroup recommendRg = (RadioGroup) findViewById(R.id.rg_recommend);
        if (recommendRg != null) {
            recommendRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    String message = "";

                    switch (checkedId) {
                        case R.id.rb_yes:
                            message = "Yes - a great movie";
                            break;
                        case R.id.rb_maybe:
                            message = "Maybe - I have seen better";
                            break;
                        case R.id.rb_no:
                            message = "No - big mistake";
                            break;
                    }

                    Toast.makeText(RadioButtonsActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        String message = "";

        switch (view.getId()) {
            case R.id.rb_starwars:
                if (checked)
                    message = "Luke, I am your father!";
                break;
            case R.id.rb_gladiator:
                if (checked)
                    message = "My name is Gladiator.";
                break;
            case R.id.rb_titanic:
                if (checked)
                    message = "I'll never let go, Jack. I'll never let go.";
                break;
            case R.id.rb_forestgump:
                if (checked)
                    message = "Run Forest!  Run!";
                break;
        }

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
