package com.androidonlinelessons.shorts.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.androidonlinelessons.shorts.R;

public class LinearLayoutOrientationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_layout_orientation);
    }
}
