package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.androidonlinelessons.shorts.R;

public class CheckboxesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkboxes);

        // alternative way to send onClick listener
        CheckBox codeCheckbox = (CheckBox) findViewById(R.id.check_code);
        if (codeCheckbox != null) {
            codeCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    onCheckboxClicked(buttonView);
                }
            });
        }
    }

    public void onCheckboxClicked(View view) {
        boolean checked = ((CheckBox) view).isChecked();
        String message = "";

        switch (view.getId()) {
            case R.id.check_gym:
                if (checked)
                    message = "I feel much stronger";
                else
                    message = "Need to keep in shape";
                break;
            case R.id.check_groceries:
                if (checked)
                    message = "I can't wait to eat";
                else
                    message = "Need to pick up something tasty";
                break;
            case R.id.check_code:
                if (checked)
                    message = "I will be famous";
                else
                    message = "There must be something cool to code";
                break;
        }

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
