package com.androidonlinelessons.shorts.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.androidonlinelessons.shorts.R;

public class SnackbarsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snackbars);
    }

    public void showSimpleSnackbar(View view) {
        CharSequence text = "Hi!  How are you?";
        int duration = Snackbar.LENGTH_SHORT;

        Snackbar snackbar = Snackbar.make(view, text, duration);
        snackbar.show();
    }

    public void showSnackbarWithAction(View view) {
        Snackbar snackbar = Snackbar
                .make(view, "Death Star was destroyed.", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(SnackbarsActivity.this, "Rebuilding...", Toast
                                .LENGTH_SHORT).show();
                    }
                });

        snackbar.show();
    }

    public void showCustomSnackbar(View view) {
        Snackbar snackbar = Snackbar
                .make(view, "Cannot download data.", Snackbar.LENGTH_INDEFINITE)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(SnackbarsActivity.this, "Retrying...", Toast
                                .LENGTH_SHORT).show();
                    }
                });

        snackbar.setActionTextColor(Color.GREEN);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id
                .snackbar_text);
        textView.setTextColor(Color.CYAN);
        snackbar.show();
    }
}
