package com.androidonlinelessons.shorts.activity;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.androidonlinelessons.shorts.R;
import com.androidonlinelessons.shorts.fragment.DatePickerFragment;
import com.androidonlinelessons.shorts.fragment.TimePickerFragment;

public class PickersActivity extends BaseActivity implements TimePickerDialog
        .OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickers);
    }

    public void showTimeDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    public void showDateDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        TextView timeText = (TextView) findViewById(R.id.txt_time);
        if (timeText != null) {
            timeText.setText("Time: " + hourOfDay + ":" + minute);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        TextView dateText = (TextView) findViewById(R.id.txt_date);
        if (dateText != null) {
            dateText.setText("Date: " + monthOfYear + "/" + dayOfMonth + "/" + year);
        }
    }
}
