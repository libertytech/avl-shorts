package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidonlinelessons.shorts.R;
import com.androidonlinelessons.shorts.adapter.IconTextOptimizedAdapter;

public class ListViewCustomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_view_custom);

        ListView customListView = (ListView) findViewById(R.id.custom_months_list);

        IconTextOptimizedAdapter adapter = new IconTextOptimizedAdapter(this, getResources()
                .getStringArray(R.array
                .months_array));
        if (customListView != null) {
            customListView.setAdapter(adapter);

            customListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    Toast.makeText(ListViewCustomActivity.this, "Item " + position,
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
