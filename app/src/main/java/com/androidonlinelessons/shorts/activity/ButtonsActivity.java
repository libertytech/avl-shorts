package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.androidonlinelessons.shorts.R;

public class ButtonsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buttons);

        ImageButton imageButton = (ImageButton) findViewById(R.id.btn_image);
        if (imageButton != null) {
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(ButtonsActivity.this, getString(R.string.play_image), Toast
                            .LENGTH_LONG).show();
                }
            });
        }

        Button imgTextButton = (Button) findViewById(R.id.btn_txt_image);
        if (imgTextButton != null) {
            imgTextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(ButtonsActivity.this, getString(R.string.play_text_image), Toast
                            .LENGTH_LONG).show();
                }
            });
        }
    }

    public void startPlaying(View view) {
        Toast.makeText(this, getString(R.string.play_text), Toast.LENGTH_LONG).show();
    }
}
