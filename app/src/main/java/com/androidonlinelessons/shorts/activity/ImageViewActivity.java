package com.androidonlinelessons.shorts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.androidonlinelessons.shorts.R;

public class ImageViewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
    }

    public void showImageViewTint(View view) {
        startActivity(new Intent(getApplicationContext(), ImageViewTintActivity
                .class));
    }

    public void showImageViewCenter(View view) {
        startActivity(new Intent(getApplicationContext(), ImageViewCenterActivity
                .class));
    }

    public void showImageViewCenterCrop(View view) {
        startActivity(new Intent(getApplicationContext(), ImageViewCenterCropActivity
                .class));
    }

    public void showImageViewCenterInside(View view) {
        startActivity(new Intent(getApplicationContext(), ImageViewCenterInsideActivity
                .class));
    }

    public void showImageViewFitCenter(View view) {
        startActivity(new Intent(getApplicationContext(), ImageViewFitCenterActivity
                .class));
    }

    public void showImageViewFitStart(View view) {
        startActivity(new Intent(getApplicationContext(), ImageViewFitStartActivity
                .class));
    }

    public void showImageViewFitEnd(View view) {
        startActivity(new Intent(getApplicationContext(), ImageViewFitEndActivity
                .class));
    }

    public void showImageViewFitXY(View view) {
        startActivity(new Intent(getApplicationContext(), ImageViewFitXYActivity
                .class));
    }
}
