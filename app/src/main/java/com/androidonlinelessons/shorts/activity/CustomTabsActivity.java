package com.androidonlinelessons.shorts.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.androidonlinelessons.shorts.R;

public class CustomTabsActivity extends BaseActivity {

    private String url = "https://www.google.com/";

    private static final int ACTION_CODE_SHARE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_custom_tabs);
    }

    public void launchCustomTabsBasic(View view) {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(url));
    }

    public void launchCustomTabsConfigurable(View view) {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();

        builder.setToolbarColor(ContextCompat.getColor(this, R.color.colorAccent));
        addShareActionIcon(url, builder);
        addShareMenuIcon(url, builder);

        builder.setStartAnimations(this, R.anim.slide_in_right_to_center, R.anim
                .slide_out_center_to_left);
        builder.setExitAnimations(this, R.anim.slide_in_left_to_center, R.anim
                .slide_out_center_to_right);

        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(url));
    }

    private void addShareActionIcon(String shareUrl, CustomTabsIntent.Builder builder) {
        Bitmap iconShare = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_share_white_24dp);

        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string
                .share_via));
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareUrl);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, ACTION_CODE_SHARE,
                shareIntent, 0);

        builder.setActionButton(iconShare, getString(R.string.share_via), pendingIntent, false);
    }

    private void addShareMenuIcon(String shareUrl, CustomTabsIntent.Builder builder) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string
                .share_via));
        shareIntent.putExtra(Intent.EXTRA_TEXT, shareUrl);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, ACTION_CODE_SHARE,
                shareIntent, 0);

        builder.addMenuItem(getString(R.string.share_via), pendingIntent);
    }
}
