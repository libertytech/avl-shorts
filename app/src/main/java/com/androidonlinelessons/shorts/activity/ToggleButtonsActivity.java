package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.androidonlinelessons.shorts.R;

public class ToggleButtonsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toggle_buttons);

        // alternative way to send onClick listener
        Switch defaultSwitch = (Switch) findViewById(R.id.sw_default);
        if (defaultSwitch != null) {
            defaultSwitch.setOnCheckedChangeListener(switchListener);
        }

        Switch customSwitch = (Switch) findViewById(R.id.sw_custom);
        if (customSwitch != null) {
            customSwitch.setOnCheckedChangeListener(switchListener);
        }
    }

    CompoundButton.OnCheckedChangeListener switchListener = new CompoundButton
            .OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            String message;
            if (isChecked) {
                message = "Checked";
            } else {
                message = "Unchecked";
            }

            Toast.makeText(ToggleButtonsActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    };

    public void onToggleButtonClicked(View view) {
        boolean checked = ((ToggleButton) view).isChecked();

        String message;
        if (checked) {
            message = "Checked";
        } else {
            message = "Unchecked";
        }

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
