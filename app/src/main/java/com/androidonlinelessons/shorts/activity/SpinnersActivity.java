package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.androidonlinelessons.shorts.R;

public class SpinnersActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinners);

        Spinner seasonSpinner = (Spinner) findViewById(R.id.sp_seasons);
        if (seasonSpinner != null) {
            seasonSpinner.setOnItemSelectedListener(listener);
        }
    }

    AdapterView.OnItemSelectedListener listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Spinner monthsSpinner = (Spinner) findViewById(R.id.sp_months);

            ArrayAdapter<CharSequence> adapter;
            switch (position) {
                case 0:
                    adapter = ArrayAdapter.createFromResource(SpinnersActivity.this,
                            R.array.months_array, android.R.layout.simple_spinner_item);
                    break;
                case 1:
                    adapter = ArrayAdapter.createFromResource(SpinnersActivity.this,
                            R.array.spring_months_array, android.R.layout.simple_spinner_item);
                    break;
                case 2:
                    adapter = ArrayAdapter.createFromResource(SpinnersActivity.this,
                            R.array.summer_months_array, android.R.layout.simple_spinner_item);
                    break;
                case 3:
                    adapter = ArrayAdapter.createFromResource(SpinnersActivity.this,
                            R.array.fall_months_array, android.R.layout.simple_spinner_item);
                    break;
                case 4:
                    adapter = ArrayAdapter.createFromResource(SpinnersActivity.this,
                            R.array.winter_months_array, android.R.layout.simple_spinner_item);
                    break;
                default:
                    adapter = ArrayAdapter.createFromResource(SpinnersActivity.this,
                            R.array.months_array, android.R.layout.simple_spinner_item);
                    break;
            }

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            if (monthsSpinner != null) {
                monthsSpinner.setAdapter(adapter);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // perform action
        }
    };
}
