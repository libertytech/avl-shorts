package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.androidonlinelessons.shorts.R;

public class WebViewConfigurableActivity extends BaseActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview_basic);

        webView = (WebView) findViewById(R.id.webview);

        // configure browser settings
        if (webView != null) {
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

            // enable responsive layout
            webView.getSettings().setUseWideViewPort(true);
            // zoom out if the content width is greater than the width of the view port
            webView.getSettings().setLoadWithOverviewMode(true);

            // configure zooming and its controls
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);

            // configure client behavior when loading url
            webView.setWebViewClient(new MyClient());

            // Load the initial URL
            webView.loadUrl("http://www.google" +
                    ".com/search?hl=en&gl=us&tbm=nws&authuser=0&q=android&oq=android");
        }
    }

    private class MyClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            Toast.makeText(WebViewConfigurableActivity.this, "Loading: " + url, Toast
                    .LENGTH_SHORT).show();

            return true;
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
