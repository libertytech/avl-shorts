package com.androidonlinelessons.shorts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.androidonlinelessons.shorts.R;

public class WebViewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview);
    }

    public void launchWebViewBasic(View view) {
        startActivity(new Intent(this,
                WebViewBasicActivity
                        .class));
    }

    public void launchWebViewConfigurable(View view) {
        startActivity(new Intent(this,
                WebViewConfigurableActivity
                        .class));
    }

    public void launchWebViewLocal(View view) {
        startActivity(new Intent(this,
                WebViewLocalActivity
                        .class));
    }

    public void launchWebViewLocalWithJavaScript(View view) {
        startActivity(new Intent(this,
                WebViewLocalJavascriptActivity
                        .class));
    }
}
