package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.androidonlinelessons.shorts.R;
import com.androidonlinelessons.shorts.adapter.TextImageAdapter;

public class GridViewCustomActivity extends AppCompatActivity {

    // references images
    private Integer[] monthsIcons = {
            R.drawable.january, R.drawable.february, R.drawable.march, R.drawable.april, R
            .drawable.may, R.drawable.june, R.drawable.july,
            R.drawable.august, R.drawable.september, R.drawable.october, R.drawable.november, R
            .drawable.december
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_grid_view_layout);

        GridView gridview = (GridView) findViewById(R.id.months_grid);
        TextImageAdapter adapter = new TextImageAdapter(this, getResources().getStringArray(R.array
                .months_array), monthsIcons);
        if (gridview != null) {
            gridview.setAdapter(adapter);

            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v,
                                        int position, long id) {
                    Toast.makeText(GridViewCustomActivity.this, "Item " + position,
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
