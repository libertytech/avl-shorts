package com.androidonlinelessons.shorts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.androidonlinelessons.shorts.R;
import com.androidonlinelessons.shorts.adapter.LessonAdapter;
import com.androidonlinelessons.shorts.model.Lesson;

import java.util.ArrayList;

public class BaseActivity extends AppCompatActivity {

    protected CoordinatorLayout topLayout;
    protected FrameLayout contentLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        topLayout = (CoordinatorLayout) getLayoutInflater().inflate(R.layout.root_layout, null);
        if (topLayout != null) {
            contentLayout = (FrameLayout) topLayout.findViewById(R.id.activity_content);
        }

        super.setContentView(topLayout);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        getLayoutInflater().inflate(layoutResID, contentLayout, true);
    }
}
