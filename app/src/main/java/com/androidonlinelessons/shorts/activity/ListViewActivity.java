package com.androidonlinelessons.shorts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.androidonlinelessons.shorts.R;

public class ListViewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        Button listDefaultButton = (Button) findViewById(R.id.btn_list_view_default);
        if (listDefaultButton != null) {
            listDefaultButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), ListViewDefaultActivity
                            .class));
                }
            });
        }

        Button listCustomButton = (Button) findViewById(R.id.btn_list_view_custom);
        if (listCustomButton != null) {
            listCustomButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), ListViewCustomActivity
                            .class));
                }
            });
        }
    }
}
