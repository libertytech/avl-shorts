package com.androidonlinelessons.shorts.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.androidonlinelessons.shorts.R;

public class LinearLayoutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_layout);

        Button linearLayoutButton = (Button) findViewById(R.id.linear_layout_orientation_button);
        if (linearLayoutButton != null) {
            linearLayoutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),
                            LinearLayoutOrientationActivity
                            .class));
                }
            });
        }

        Button linearLayoutWeightEqualButton = (Button) findViewById(R.id
                .linear_layout_weight_equal_button);
        if (linearLayoutWeightEqualButton != null) {
            linearLayoutWeightEqualButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),
                            LinearLayoutWeightEqualActivity
                            .class));
                }
            });
        }

        Button linearLayoutWeightVariedButton = (Button) findViewById(R.id
                .linear_layout_weight_varied_button);
        if (linearLayoutWeightVariedButton != null) {
            linearLayoutWeightVariedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),
                            LinearLayoutWeightVariedActivity
                            .class));
                }
            });
        }
    }
}
