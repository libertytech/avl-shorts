package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.androidonlinelessons.shorts.R;

public class ActivityB extends BaseActivity {

    public static final String TAG = ActivityB.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, ">>> onCreate - The activity is being created.");

        setContentView(R.layout.activity_b);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, ">>>>>> onRestart - The activity is restarting.");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, ">>>>>> onStart - The activity is about to become visible.");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, ">>>>>>>>> - onResume - The activity has become visible (it is now " +
                "\"resumed\").");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, ">>>>>>>>> - onPause - Another activity is taking focus (this activity is " +
                "about to be \"paused\").");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, ">>>>>> - onStop - The activity is no longer visible (it is now \"stopped\")");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, ">>> - onDestroy - The activity is about to be destroyed.");
    }

    public void goBack(View view) {
        super.onBackPressed();
    }
}
