package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.androidonlinelessons.shorts.R;

public class WebViewBasicActivity extends BaseActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview_basic);

        webView = (WebView) findViewById(R.id.webview);

        if (webView != null) {
            webView.setWebViewClient(new WebViewClient());
            webView.loadUrl("http://www.google.com");
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
