package com.androidonlinelessons.shorts.activity;

import android.os.Bundle;
import android.view.View;

import com.androidonlinelessons.shorts.R;

public class ImageViewCenterActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view_center);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
